from PyQt4 import QtCore, QtGui
import messages

class Ui_dialog(QtGui.QDialog):
    title = " "
    text = ""

    def __init__(self):
        QtGui.QDialog.__init__(self)
        self.setupUi()
        
    def setupUi(self):
       
        self.setObjectName("dialog")
        self.resize(QtCore.QSize(QtCore.QRect(0, 0, 380, 100).size()).expandedTo(self.minimumSizeHint()))
        self.setWindowTitle(self.title)
        
        self.line = QtGui.QLineEdit(self)
        self.line.setGeometry(QtCore.QRect(10, 10, 360, 20))
        self.line.setText(self.text)

        self.buttonBox = QtGui.QDialogButtonBox(self)
        self.buttonBox.setGeometry(QtCore.QRect(170, 60, 200, 32))
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtGui.QDialogButtonBox.Cancel|QtGui.QDialogButtonBox.NoButton|QtGui.QDialogButtonBox.Ok)
        self.buttonBox.setObjectName("buttonBox")
        
        QtCore.QObject.connect(self.buttonBox,QtCore.SIGNAL("accepted()"),self.accept)
        QtCore.QObject.connect(self.buttonBox,QtCore.SIGNAL("rejected()"),self.reject)
        QtCore.QMetaObject.connectSlotsByName(self)
        
if __name__ == '__main__':
    app = QtGui.QApplication([])
    app.setStyle("Cleanlooks")
    dialog = Ui_dialog()
    result = dialog.exec_()
    if result:
        #print dialog.setupUi.memo.text()
        print 'Ok Button pressed'
    else: 
        print 'Cancel Button pressed'
    