from PyQt4 import QtCore, QtGui
import csvtable

reference = []
providers = []
cur_refer = 0
cur_prov = 0
price = 0
count = 0    

class Ui_dialog(QtGui.QDialog):
    label_prov = "Provider"
    label_name = "Name"
    label_price = "Price"
    label_count = "Count"
    tblRefer = csvtable.csvtable()
    tblProv = csvtable.csvtable()
    
    def __init__(self):
        QtGui.QDialog.__init__(self)
        self.setupUi()
   
    def setupUi(self):
        self.tblProv.setTable(globals()['providers'])
        self.tblRefer.setTable(globals()['reference'])

        self.setObjectName("dialog")
        self.resize(QtCore.QSize(QtCore.QRect(0,0,400,200).size()).expandedTo(self.minimumSizeHint()))

        self.buttonBox = QtGui.QDialogButtonBox(self)
        self.buttonBox.setGeometry(QtCore.QRect(50,165,341,32))
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtGui.QDialogButtonBox.Cancel|QtGui.QDialogButtonBox.NoButton|QtGui.QDialogButtonBox.Ok)
        self.buttonBox.setObjectName("buttonBox")
        
        self.label0 = QtGui.QLabel(self)
        self.label0.setGeometry(QtCore.QRect(10,10,211,16))
        self.label0.setObjectName("label")
        self.label0.setText(self.label_prov)
        self.Prov_comboBox = QtGui.QComboBox(self)
        self.Prov_comboBox.setGeometry(QtCore.QRect(10,30,381,22))
        self.Prov_comboBox.setObjectName("comboBox")

        self.tblProv.First()
        cur_index = 0
        for row in range(0, self.tblProv.RecCount()):
            self.Prov_comboBox.addItem(self.tblProv.getField('name'))
            if int(globals()['cur_prov']) == int(self.tblProv.getField('id')) and int(globals()['cur_prov']) != 0:
                cur_index = row
            elif int(globals()['cur_prov']) == 0:
                cur_index = 0
            self.tblProv.Next()  
        self.Prov_comboBox.setCurrentIndex(cur_index)

        self.label = QtGui.QLabel(self)
        self.label.setGeometry(QtCore.QRect(10,60,211,16))
        self.label.setObjectName("label")
        self.label.setText(self.label_name)
        self.comboBox = QtGui.QComboBox(self)
        self.comboBox.setGeometry(QtCore.QRect(10,80,381,22))
        self.comboBox.setObjectName("comboBox")
        
        self.tblRefer.First()
        cur_index = 0
        for row in range(0, self.tblRefer.RecCount()):
            self.comboBox.addItem(self.tblRefer.getField('name'))
            if int(globals()['cur_refer']) == int(self.tblRefer.getField('id')) and int(globals()['cur_prov']) != 0:
                cur_index = row
            elif int(globals()['cur_refer']) == 0:
                cur_index = 0                
            self.tblRefer.Next()  
        self.comboBox.setCurrentIndex(cur_index)


        self.label_2 = QtGui.QLabel(self)
        self.label_2.setGeometry(QtCore.QRect(210,110,171,16))
        self.label_2.setObjectName("label_2")
        self.label_2.setText(self.label_price)
        
        self.label_3 = QtGui.QLabel(self)
        self.label_3.setGeometry(QtCore.QRect(10,110,71,16))
        self.label_3.setObjectName("label_3")
        self.label_3.setText(self.label_count)
        
        
        self.price_SpinBox = QtGui.QDoubleSpinBox(self)
        self.price_SpinBox.setGeometry(QtCore.QRect(210,130,171,22))
        self.price_SpinBox.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.price_SpinBox.setMaximum(999999999.99)
        self.price_SpinBox.setObjectName("price_SpinBox")
        self.price_SpinBox.setProperty("value",QtCore.QVariant(globals()['price']))

        self.count_SpinBox = QtGui.QDoubleSpinBox(self)
        self.count_SpinBox.setGeometry(QtCore.QRect(10,130,181,22))
        self.count_SpinBox.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.count_SpinBox.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.count_SpinBox.setButtonSymbols(QtGui.QAbstractSpinBox.UpDownArrows)
        self.count_SpinBox.setDecimals(0)
        self.count_SpinBox.setMaximum(99999999.0)
        self.count_SpinBox.setObjectName("count_SpinBox")
        self.count_SpinBox.setProperty("value",QtCore.QVariant(globals()['count']))
        
        QtCore.QObject.connect(self.buttonBox,QtCore.SIGNAL("accepted()"),self.accept)
        QtCore.QObject.connect(self.buttonBox,QtCore.SIGNAL("rejected()"),self.reject)
        QtCore.QObject.connect(self.count_SpinBox,QtCore.SIGNAL("valueChanged(double)"),self.setCount)
        QtCore.QObject.connect(self.price_SpinBox,QtCore.SIGNAL("valueChanged(double)"),self.setPrice)
        QtCore.QObject.connect(self.comboBox,QtCore.SIGNAL("currentIndexChanged(int)"),self.setName)
        QtCore.QObject.connect(self.Prov_comboBox,QtCore.SIGNAL("currentIndexChanged(int)"),self.setProv)
        QtCore.QMetaObject.connectSlotsByName(self)
        self.setCount()
        self.setPrice()
        self.setName()
        self.setProv()

    def setCount(self):
        globals()['count'] = int(self.count_SpinBox.value()) 
    def setPrice(self):
        globals()['price'] = float(self.price_SpinBox.value()) 
    def setName(self):
        self.tblRefer.setTable(globals()['reference'])
        self.tblRefer.index = self.comboBox.currentIndex()
        globals()['cur_refer'] = int(self.tblRefer.getField('id'))
    def setProv(self):
        self.tblProv.setTable(globals()['providers'])
        self.tblProv.index = self.Prov_comboBox.currentIndex()
        globals()['cur_prov'] = int(self.tblProv.getField('id'))

        
if __name__ == '__main__':
    tblRefer = csvtable.csvtable()
    tblRefer.LoadFromFile('reference.csv')
    globals()['reference'] = tblRefer.getTable()
    
    tblProv = csvtable.csvtable()
    tblProv.LoadFromFile('providers.csv')
    globals()['providers'] = tblProv.getTable()
    
    globals()['count'] = 3
    globals()['price'] = 5.5
    globals()['cur_refer'] = 12
    globals()['cur_prov'] = 7
    
    app = QtGui.QApplication([])
    dialog = Ui_dialog()
    result = dialog.exec_()

    #if result: print 'OK Button pressed'
    #else: print 'Cancel Button pressed'