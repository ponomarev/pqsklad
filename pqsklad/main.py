# -*- coding: cp1251 -*-
import sys, string, time
from PyQt4 import QtCore, QtGui
import time
import messages
import csvtable
import itemedit
import referedit
import provedit
import rcptedit
import balance
import txtout
import lineedit
import prodedit
import mdata
import newincome
import newoutcome
import printout

cur_view = None

def insmes(txt):
    return unicode(messages.messages[txt], "cp1251")

def del_chst(s):
    t = ''
    for i in s:
        if i != '\n': t = t + i
    return t

class MainWindow(QtGui.QMainWindow):
    def __init__(self, parent=None):
        QtGui.QMainWindow.__init__(self, parent)

        self.setWindowTitle(insmes('TITLE_MN_WN'))
        #self.showMaximized()

        centralWidget = QtGui.QWidget()

        btnDirs = QtGui.QPushButton()
        btnDirs.setText(insmes('TITLE_DIRS'))

        btnRefer = QtGui.QPushButton()
        btnRefer.setText(insmes('TITLE_REFERENCE'))

        btnProv = QtGui.QPushButton()
        btnProv.setText(insmes('ITEM_PROV'))

        btnRcpt = QtGui.QPushButton()
        btnRcpt.setText(insmes('ITEM_RCPT'))

        btnBalance = QtGui.QPushButton()
        btnBalance.setText(insmes('TITLE_MN_WN'))

        btnIncome = QtGui.QPushButton()
        btnIncome.setText(insmes('TITLE_INCOME'))

        btnAdd = QtGui.QPushButton()
        btnAdd.setText('+')

        btnDel = QtGui.QPushButton()
        btnDel.setText('-')

        btnOutcome = QtGui.QPushButton()
        btnOutcome.setText(insmes('TITLE_OUTCOME'))

        self.dateEdit = QtGui.QDateEdit(self)
        #self.dateEdit.setGeometry(QtCore.QRect(30,20,110,22))
        self.dateEdit.setButtonSymbols(QtGui.QAbstractSpinBox.UpDownArrows)
        self.dateEdit.setCalendarPopup(True)
        self.dateEdit.setObjectName("dateEdit")

        btnTxtout = QtGui.QPushButton()
        btnTxtout.setText(insmes('TITLE_TXTOUT'))
        btnCsvout = QtGui.QPushButton()
        btnCsvout.setText(insmes('TITLE_CSVOUT'))

        btnPrint = QtGui.QPushButton()
        btnPrint.setText(insmes('PRINT'))

        self.table = QtGui.QTableWidget(0, 0, self)
        self.table.setAlternatingRowColors(True)
        #self.table.setSortingEnabled(True)
        #self.table.horizontalHeader().resizeSection(0, 300)
        self.table.setEditTriggers(QtGui.QAbstractItemView.NoEditTriggers)

        #headerItem = QtGui.QTableWidgetItem()
        #headerItem.setText(unicode('Наименование', "cp1251")) 
        #headerItem.baseSize = 700
        #self.table.setHorizontalHeaderItem(0,headerItem)

        #headerItem1 = QtGui.QTableWidgetItem()
        #headerItem1.setText(QtGui.QApplication.translate("MainWindow", "Count", None, QtGui.QApplication.UnicodeUTF8))
        #self.table.setHorizontalHeaderItem(1,headerItem1)    

        #headerItem2 = QtGui.QTableWidgetItem()
        #headerItem2.setText(QtGui.QApplication.translate("MainWindow", "Price", None, QtGui.QApplication.UnicodeUTF8))
        #self.table.setHorizontalHeaderItem(2,headerItem2)    

        controlsLayout = QtGui.QHBoxLayout()
        #setTextAlignment(QtCore.Qt.AlignLeft)
        #controlsLayout.setTextAlignment(QtCore.Qt.AlignLeft)
        controlsLayout.addStretch(1)
        controlsLayout.addWidget(btnDirs)
        #controlsLayout.addWidget(btnRefer)
        #controlsLayout.addWidget(btnProv)
        #controlsLayout.addWidget(btnRcpt)
        #controlsLayout.addSpacing(24)
        controlsLayout.addWidget(btnBalance)
        #controlsLayout.addSpacing(24)
        controlsLayout.addWidget(btnIncome)
        controlsLayout.addWidget(btnAdd)
        controlsLayout.addWidget(btnDel)
        controlsLayout.addWidget(btnOutcome)

        #controlsLayout.addSpacing(24)
        controlsLayout.addWidget(self.dateEdit)
        #controlsLayout.addSpacing(24)
        #controlsLayout.addWidget(btnTxtout)
        controlsLayout.addWidget(btnPrint)

        controlsLayout.addStretch(1)

        centralLayout = QtGui.QVBoxLayout()
        centralLayout.addLayout(controlsLayout)
        centralLayout.addWidget(self.table, 1)
        centralWidget.setLayout(centralLayout)

        self.setCentralWidget(centralWidget)

        #        self.exit = QtGui.QAction(QtGui.QIcon('icons/exit.png'), 'Exit', self)
        #        self.exit.setShortcut('Ctrl+T')
        #        self.connect(self.exit, QtCore.SIGNAL('triggered()'), self.save_data)
        #        self.toolbar = self.addToolBar('Exit')
        #        self.toolbar.addAction(self.exit)

        self.connect(btnDirs, QtCore.SIGNAL("clicked()"), self.show_directories_table)

        self.connect(btnRefer, QtCore.SIGNAL("clicked()"), self.list_reference)
        btnRefer.setShortcut('Ctrl+R')

        self.connect(btnProv, QtCore.SIGNAL("clicked()"), self.show_providers_table)
        btnProv.setShortcut('Ctrl+P')

        self.connect(btnRcpt, QtCore.SIGNAL("clicked()"), self.list_recipients)
        self.connect(btnBalance, QtCore.SIGNAL("clicked()"), self.show_balance_table)

        self.connect(btnIncome, QtCore.SIGNAL("clicked()"), self.show_income_table)
        btnIncome.setShortcut('Ctrl+I')

        self.connect(btnAdd, QtCore.SIGNAL("clicked()"), self.add_item)
        btnAdd.setShortcut('Ctrl+Insert')
        self.connect(btnDel, QtCore.SIGNAL("clicked()"), self.del_item)
        btnDel.setShortcut('Ctrl+Delete')

        self.connect(btnOutcome, QtCore.SIGNAL("clicked()"), self.show_outcome_table)
        btnOutcome.setShortcut('Ctrl+O')

        self.connect(btnPrint, QtCore.SIGNAL("clicked()"), self.make_report)

        self.connect(self.table, QtCore.SIGNAL("doubleClicked(QModelIndex)"), self.show_item_edit)

    #self.connect(self.table,QtCore.SIGNAL("itemChanged(QTableWidgetItem*)"),self.show_item_edit)

    def make_report(self):
        if self.view_mode() == 'income':
            printout.income_table(self.get_cur_date())

    def view_mode(self, mode=""):
        if mode == "": return globals()['cur_view']
        if mode == 'providers':
            globals()['cur_view'] = 'providers'
            self.setWindowTitle(insmes('TITLE_PROV') + ' - ' + insmes('TITLE_MN_WN'))
        elif mode == 'recipients':
            globals()['cur_view'] = 'recipients'
            self.setWindowTitle(insmes('TITLE_RCPT') + ' - ' + insmes('TITLE_MN_WN'))
        elif mode == 'directories':
            globals()['cur_view'] = 'directories'
            self.setWindowTitle(insmes('TITLE_DIRS') + ' - ' + insmes('TITLE_MN_WN'))
        elif mode == 'products_groups':
            globals()['cur_view'] = 'products_groups'
            self.setWindowTitle(insmes('TITLE_GPROD') + ' - ' + insmes('TITLE_MN_WN'))
        elif mode == 'products':
            globals()['cur_view'] = 'products'
            self.setWindowTitle(insmes('TITLE_PROD') + ' - ' + insmes('TITLE_MN_WN'))
        elif mode == 'income':
            globals()['cur_view'] = 'income'
            self.setWindowTitle(insmes('TITLE_INC') + ' - ' + insmes('TITLE_MN_WN'))
        elif mode == 'outcome':
            globals()['cur_view'] = 'outcome'
            self.setWindowTitle(insmes('TITLE_OUT') + ' - ' + insmes('TITLE_MN_WN'))


    def add_item(self):
        if self.view_mode() == 'income':
            newincome.dialog_window.date = self.get_cur_date()
            dialog = newincome.dialog_window()
            result = dialog.exec_()
            self.show_income_table()
        elif self.view_mode() == 'outcome':
            newoutcome.dialog_window.date = self.get_cur_date()
            dialog = newoutcome.dialog_window()
            result = dialog.exec_()
            self.show_outcome_table()

        elif self.view_mode() == 'products':
            prgr4out = []
            for r in mdata.get_products_groups():
                prgr4out.append((int(r['id']), r['name']))
            prodedit.dialog_window.product_group = prgr4out
            prodedit.dialog_window.cur_product_group = 0
            prodedit.dialog_window.product_name = ""
            prodedit.dialog_window.product_unit = ""
            dialog = prodedit.dialog_window()
            result = dialog.exec_()
            # FIXME: product groups non updated 
            if result:
                mdata.add_products(dialog.cur_product_group, dialog.product_name,
                                   dialog.product_unit)
                self.show_products_table()
        elif self.view_mode() == 'products_groups':
            lineedit.Ui_dialog.title = ""
            lineedit.Ui_dialog.text = ""
            dialog = lineedit.Ui_dialog()
            result = dialog.exec_()
            if result:
                mdata.add_products_groups(dialog.line.text())
                self.show_products_groups_table()

        elif self.view_mode() == 'providers':
            provedit.Ui_dialog.title = ""
            provedit.Ui_dialog.labelShortName = ""
            provedit.Ui_dialog.labelFullName = ""
            provedit.Ui_dialog.labelAddress = ""
            provedit.Ui_dialog.labelInn = ""
            provedit.Ui_dialog.labelKpp = ""
            provedit.Ui_dialog.labelAcc = ""
            provedit.Ui_dialog.labelBnk = ""
            dialog = provedit.Ui_dialog()
            result = dialog.exec_()
            if result:
                mdata.add_provider(
                        short_name=dialog.editShortName.text(),
                        full_name=dialog.editFullName.text(),
                        address=dialog.editAddress.text(),
                        inn=dialog.editInn.text(),
                        kpp=dialog.editKpp.text(),
                        account=dialog.editAcc.text(),
                        bank=dialog.editBank.text()
                        )
                self.show_providers_table()
        elif self.view_mode() == 'recipients':
            rcptedit.Ui_dialog.title = ""
            rcptedit.Ui_dialog.labelShortName = ""
            rcptedit.Ui_dialog.labelFullName = ""
            rcptedit.Ui_dialog.labelAddress = ""
            rcptedit.Ui_dialog.labelInn = ""
            rcptedit.Ui_dialog.labelKpp = ""
            rcptedit.Ui_dialog.labelAcc = ""
            rcptedit.Ui_dialog.labelBnk = ""
            dialog = rcptedit.Ui_dialog()
            result = dialog.exec_()
            if result:
                mdata.add_recipient(
                        short_name=dialog.editShortName.text(),
                        full_name=dialog.editFullName.text(),
                        address=dialog.editAddress.text(),
                        inn=dialog.editInn.text(),
                        kpp=dialog.editKpp.text(),
                        account=dialog.editAcc.text(),
                        bank=dialog.editBank.text()
                        )
                self.show_recipients_table()


    def del_item(self):
        if self.view_mode() == 'income':
            cur_row = self.table.currentRow()
            mdata.del_income(int(self.get_cell_value(cur_row, 0)))
            self.table.removeRow(cur_row)
        elif self.view_mode() == 'products':
            cur_row = self.table.currentRow()
            mdata.del_products(int(self.get_cell_value(cur_row, 0)))
            self.table.removeRow(cur_row)
        elif self.view_mode() == 'products_groups':
            cur_row = self.table.currentRow()
            mdata.del_products_groups(int(self.get_cell_value(cur_row, 0)))
            self.table.removeRow(cur_row)
        elif self.view_mode() == 'providers':
            cur_row = self.table.currentRow()
            mdata.del_provider(int(self.get_cell_value(cur_row, 0)))
            self.table.removeRow(cur_row)


    def set_cur_date(self, cur_date=''):
        if cur_date == '':
            y = time.gmtime()[0]
            m = time.gmtime()[1]
            d = time.gmtime()[2]
        else:
            y = int(cur_date[0:4])
            m = int(cur_date[4:6])
            d = int(cur_date[6:8])
        self.dateEdit.setDateTime(QtCore.QDateTime(QtCore.QDate(y, m, d), QtCore.QTime(0, 0, 0)))

    def get_cur_date(self):
        d = QtCore.QDate()
        d = self.dateEdit.date()
        y = d.year()
        m = d.month()
        d = d.day()
        return str(y) + str(m) + str(d)


    def save_reference(self):
        out = ''
        for i in globals()['reference']:
            line = del_chst(str(i[0]) + ';' + i[1]) + ';\n'
            out += line
        open('reference.csv', 'w').write(out)

    def save_providers(self):
        out = ''
        for i in globals()['providers']:
            line = del_chst(str(i[0]) + ';' + i[1]) + ';\n'
            out += line
        open('providers.csv', 'w').write(out)

    def save_income(self):
        out = ''
        for i in globals()['income']:
            line = del_chst(i[0] + ';' + i[1] + ';'\
                            + i[2] + ';' + i[3] + ';' + i[4]) + ';\n'
            out += line
        open('income.csv', 'w').write(out)

    def list_reference(self):
        tblRefer = csvtable.csvtable()
        tblRefer.LoadFromFile('reference.csv')
        referedit.reference = tblRefer.getTable()
        referedit.key = 'id'
        dialog = referedit.Ui_dialog()
        result = dialog.exec_()
        tblRefer.Clear()
        if result:
            tblRefer.setTable(referedit.reference)
            tblRefer.SaveToFile('reference.csv')

    def list_providers(self):
        providers = mdata.get_providers()


    #tblProv = csvtable.csvtable()
    #tblProv.LoadFromFile('providers.csv')
    #provedit.reference = tblProv.getTable()
    #provedit.key = 'id'
    #dialog = provedit.Ui_dialog()
    #result = dialog.exec_()
    #tblProv.Clear()
    #if result:
    #    tblProv.setTable(provedit.reference)
    #    tblProv.SaveToFile('providers.csv')

    def list_recipients(self):
        tblRcpt = csvtable.csvtable()
        tblRcpt.LoadFromFile('recipients.csv')
        rcptedit.reference = tblRcpt.getTable()
        rcptedit.key = 'id'
        dialog = rcptedit.Ui_dialog()
        result = dialog.exec_()
        tblRcpt.Clear()
        if result:
            tblRcpt.setTable(rcptedit.reference)
            tblRcpt.SaveToFile('recipients.csv')

    def get_cell_value(self, row, col):
        item = QtGui.QTableWidgetItem()
        item = self.table.item(row, col)
        try: return item.text()
        except: return ""

    def set_cell_value(self, row, col, val):
        item = QtGui.QTableWidgetItem()
        text = unicode(str(val), "cp1251")
        item.setTextAlignment(QtCore.Qt.AlignLeft)
        item.setText(text)
        self.table.setItem(row, col, item)

    def save_data(self):
        for i in range(0, self.table.rowCount()):
            if self.get_cell_value(i, 0) != "":
                print self.get_cell_value(i, 0) + ";"\
                      + self.get_cell_value(i, 1) + ";"\
                + self.get_cell_value(i, 2)

    def load_reference_data(self):
        tblRefer = csvtable.csvtable()
        tblRefer.LoadFromFile('reference.csv')
        tblRefer.Print()

    #tblRefer.Lo

    #globals()['reference'] = []
    #f = open('reference.csv', 'r')
    #for line in f:
    #    rec = [line.split(";")[0], unicode(line.split(";")[1])]
    #    globals()['reference'].append(rec)
    #f.close()

    def load_income_data(self):
        globals()['income'] = []
        f = open('income.csv', 'r')
        for line in f:
            rec = [line.split(";")[0], line.split(";")[1], line.split(";")[2],\
                   line.split(";")[3], line.split(";")[4]]
            globals()['income'].append(rec)
        f.close()

    def load_providers_data(self):
        globals()['providers'] = []
        f = open('providers.csv', 'r')
        for line in f:
            rec = [line.split(";")[0], unicode(line.split(";")[1])]
            globals()['providers'].append(rec)
        f.close()

    def get_prov_name(self, id):
        tblProv = csvtable.csvtable()
        tblProv.LoadFromFile('providers.csv')
        tblProv.First()
        for rec in range(0, tblProv.RecCount()):
            if id == tblProv.getField('id'): return tblProv.getField('name')
            tblProv.Next()

    def get_rcpt_name(self, id):
        tblRcpt = csvtable.csvtable()
        tblRcpt.LoadFromFile('recipients.csv')
        tblRcpt.First()
        for rec in range(0, tblRcpt.RecCount()):
            if id == tblRcpt.getField('id'): return tblRcpt.getField('name')
            tblRcpt.Next()

    def get_refer_name(self, id):
        tblRefer = csvtable.csvtable()
        tblRefer.LoadFromFile('reference.csv')
        tblRefer.First()
        for rec in range(0, tblRefer.RecCount()):
            if id == tblRefer.getField('id'):
                return tblRefer.getField('name')
            tblRefer.Next()

    def show_directories_table(self):
        self.view_mode('directories')
        # Marking main table
        columns = (
        ("ID", 0),
        ("TITLE_DIR", 600))
        self.table.clear()
        self.table.setColumnCount(len(columns))
        self.table.setRowCount(0)
        for c in range(len(columns)):
            headerItem0 = QtGui.QTableWidgetItem()
            headerItem0.setText(insmes(columns[c][0]))
            self.table.setHorizontalHeaderItem(c, headerItem0)
            self.table.horizontalHeader().resizeSection(c, columns[c][1])
        # filling table
        dirs = ('TITLE_PROV', 'TITLE_RCPT', 'TITLE_GPROD', 'TITLE_PROD')
        self.table.setRowCount(len(dirs))
        for i in xrange(len(dirs)):
            item = QtGui.QTableWidgetItem()
            item.setText(dirs[i])
            self.table.setItem(i, 0, item)
            del(item)
            item = QtGui.QTableWidgetItem()
            item.setText(insmes(dirs[i]))
            self.table.setItem(i, 1, item)

    def show_products_groups_table(self):
        self.view_mode('products_groups')
        # Marking main table
        columns = (
        ("ID", 0),
        ("TITLE_GPROD_NAME", 600))
        self.table.clear()
        self.table.setColumnCount(len(columns))
        self.table.setRowCount(0)
        for c in range(len(columns)):
            headerItem0 = QtGui.QTableWidgetItem()
            headerItem0.setText(insmes(columns[c][0]))
            self.table.setHorizontalHeaderItem(c, headerItem0)
            self.table.horizontalHeader().resizeSection(c, columns[c][1])
        # filling table
        providers = mdata.get_products_groups()
        #self.table.setRowCount(len(providers))
        for l in range(len(providers)):
        # add line
            self.table.insertRow(self.table.rowCount())
            # insert provider ID
            item = QtGui.QTableWidgetItem()
            item.setText(str(providers[l]['id']))
            self.table.setItem(l, 0, item)
            # insert provider short name
            del(item)
            item = QtGui.QTableWidgetItem()
            item.setText(providers[l]['name'])
            self.table.setItem(l, 1, item)


    def show_products_table(self):
        self.view_mode('products')
        # Marking main table
        columns = (
        ("ID", 0),
        ("PROD_GROUP", 200),
        ("PROD_UNIT", 100),
        ("PROD_NAME", 300))
        self.table.clear()
        self.table.setColumnCount(len(columns))
        self.table.setRowCount(0)
        for c in range(len(columns)):
            headerItem0 = QtGui.QTableWidgetItem()
            headerItem0.setText(insmes(columns[c][0]))
            self.table.setHorizontalHeaderItem(c, headerItem0)
            self.table.horizontalHeader().resizeSection(c, columns[c][1])
        # filling table
        products = mdata.get_products()
        for l in xrange(len(products)):
        # add line
            self.table.insertRow(self.table.rowCount())
            # insert product ID
            item = QtGui.QTableWidgetItem()
            item.setText(str(products[l]['id']))
            self.table.setItem(l, 0, item)
            # insert product group
            item = QtGui.QTableWidgetItem()
            item.setText(mdata.get_product_group(products[l]['product_group']))
            self.table.setItem(l, 1, item)
            # insert product unit
            item = QtGui.QTableWidgetItem()
            item.setText((products[l]['unit']))
            self.table.setItem(l, 2, item)
            # insert product name
            item = QtGui.QTableWidgetItem()
            item.setText((products[l]['name']))
            self.table.setItem(l, 3, item)

    def show_recipients_table(self):
        self.view_mode('recipients')
        # Marking main table
        columns = (
        ("ID", 0),
        ("RCPT_SH_NAME", 200),
        ("RCPT_FL_NAME", 0),
        ("RCPT_ADDRESS", 200),
        ("RCPT_INN", 150),
        ("RCPT_KPP", 100),
        ("RCPT_ACC", 200),
        ("RCPT_BNK", 250))
        self.table.clear()
        self.table.setColumnCount(len(columns))
        self.table.setRowCount(0)
        for c in range(len(columns)):
            headerItem0 = QtGui.QTableWidgetItem()
            headerItem0.setText(insmes(columns[c][0]))
            self.table.setHorizontalHeaderItem(c, headerItem0)
            self.table.horizontalHeader().resizeSection(c, columns[c][1])
        # filling table
        recipients = mdata.get_recipients()
        #self.table.setRowCount(len(providers))
        for l in range(len(recipients)):
        # add line
            self.table.insertRow(self.table.rowCount())
            # insert provider ID
            item = QtGui.QTableWidgetItem()
            item.setText(str(recipients[l]['id']))
            self.table.setItem(l, 0, item)
            # insert provider short name
            del(item)
            item = QtGui.QTableWidgetItem()
            item.setText(recipients[l]['short_name'])
            self.table.setItem(l, 1, item)
            # insert provider full name
            del(item)
            item = QtGui.QTableWidgetItem()
            item.setText(recipients[l]['full_name'])
            self.table.setItem(l, 2, item)
            # insert provider address
            del(item)
            item = QtGui.QTableWidgetItem()
            item.setText(recipients[l]['address'])
            self.table.setItem(l, 3, item)
            # insert provider inn
            del(item)
            item = QtGui.QTableWidgetItem()
            item.setText(recipients[l]['inn'])
            self.table.setItem(l, 4, item)
            # insert provider kpp
            del(item)
            item = QtGui.QTableWidgetItem()
            item.setText(recipients[l]['kpp'])
            self.table.setItem(l, 5, item)
            # insert provider acc
            del(item)
            item = QtGui.QTableWidgetItem()
            item.setText(recipients[l]['account'])
            self.table.setItem(l, 6, item)
            # insert provider bank
            del(item)
            item = QtGui.QTableWidgetItem()
            item.setText(recipients[l]['bank'])
            self.table.setItem(l, 7, item)


    def show_providers_table(self):
        self.view_mode('providers')
        # Marking main table
        columns = (
        ("ID", 0),
        ("PROV_SH_NAME", 200),
        ("PROV_FL_NAME", 0),
        ("PROV_ADDRESS", 200),
        ("PROV_INN", 150),
        ("PROV_KPP", 100),
        ("PROV_ACC", 200),
        ("PROV_BNK", 250))
        self.table.clear()
        self.table.setColumnCount(len(columns))
        self.table.setRowCount(0)
        for c in range(len(columns)):
            headerItem0 = QtGui.QTableWidgetItem()
            headerItem0.setText(insmes(columns[c][0]))
            self.table.setHorizontalHeaderItem(c, headerItem0)
            self.table.horizontalHeader().resizeSection(c, columns[c][1])
        # filling table
        providers = mdata.get_providers()
        #self.table.setRowCount(len(providers))
        for l in range(len(providers)):
        # add line
            self.table.insertRow(self.table.rowCount())
            # insert provider ID
            item = QtGui.QTableWidgetItem()
            item.setText(str(providers[l]['id']))
            self.table.setItem(l, 0, item)
            # insert provider short name
            del(item)
            item = QtGui.QTableWidgetItem()
            item.setText(providers[l]['short_name'])
            self.table.setItem(l, 1, item)
            # insert provider full name
            del(item)
            item = QtGui.QTableWidgetItem()
            item.setText(providers[l]['full_name'])
            self.table.setItem(l, 2, item)
            # insert provider address
            del(item)
            item = QtGui.QTableWidgetItem()
            item.setText(providers[l]['address'])
            self.table.setItem(l, 3, item)
            # insert provider inn
            del(item)
            item = QtGui.QTableWidgetItem()
            item.setText(providers[l]['inn'])
            self.table.setItem(l, 4, item)
            # insert provider kpp
            del(item)
            item = QtGui.QTableWidgetItem()
            item.setText(providers[l]['kpp'])
            self.table.setItem(l, 5, item)
            # insert provider acc
            del(item)
            item = QtGui.QTableWidgetItem()
            item.setText(providers[l]['account'])
            self.table.setItem(l, 6, item)
            # insert provider bank
            del(item)
            item = QtGui.QTableWidgetItem()
            item.setText(providers[l]['bank'])
            self.table.setItem(l, 7, item)


    def show_outcome_table(self):
        self.view_mode('outcome')
        # Marking main table
        columns = (
            ("ID", 50),
            ("ITEM_RCPT", 150),
            ("ITEM_NAME", 150),
            ("ITEM_COUNT", 150),
            ("ITEM_PRICE", 150),
        )
        self.table.clear()
        self.table.setColumnCount(len(columns))
        self.table.setRowCount(0)
        for c in range(len(columns)):
            headerItem0 = QtGui.QTableWidgetItem()
            headerItem0.setText(insmes(columns[c][0]))
            self.table.setHorizontalHeaderItem(c, headerItem0)
            self.table.horizontalHeader().resizeSection(c, columns[c][1])
        # filling table
        outcome = mdata.get_outcome(self.get_cur_date())
        for l in range(len(outcome)):
        # add line
            self.table.insertRow(self.table.rowCount())

            # insert outcome ID
            item = QtGui.QTableWidgetItem()
            item.setText(str(outcome[l]['id']))
            self.table.setItem(l, 0, item)

            # insert recipient name
            del(item)
            item = QtGui.QTableWidgetItem()
            item.setText(mdata.get_recipient_short_name(outcome[l]['recipient']))
            self.table.setItem(l, 1, item)

            # insert product name
            del(item)
            item = QtGui.QTableWidgetItem()
            item.setText(mdata.get_product_name(outcome[l]['product']))
            self.table.setItem(l, 2, item)

            # insert count
            del(item)
            item = QtGui.QTableWidgetItem()
            item.setText(str(outcome[l]['count']))
            self.table.setItem(l, 3, item)

            # insert price
            del(item)
            item = QtGui.QTableWidgetItem()
            item.setText(str(outcome[l]['price']))
            self.table.setItem(l, 4, item)


        return
        tblRefer = csvtable.csvtable()
        tblRcpt = csvtable.csvtable()
        tblOutcome = csvtable.csvtable()
        tblRefer.LoadFromFile('reference.csv')
        tblRcpt.LoadFromFile('recipients.csv')
        tblOutcome.LoadFromFile('outcome.csv')
        #Filter by cur_date 
        tblOutcome.Filter(True, 'date', self.get_cur_date())
        tblOutcome.First()
        self.table.setRowCount(tblOutcome.RecCount())
        for rec in range(0, tblOutcome.RecCount()):
        # insert provider name
            item = QtGui.QTableWidgetItem()
            item.setText(self.get_rcpt_name(tblOutcome.getField('recipient')))
            self.table.setItem(rec, 0, item)
            # insert refer name
            item = QtGui.QTableWidgetItem()
            item.setText(self.get_refer_name(tblOutcome.getField('reference')))
            self.table.setItem(rec, 1, item)
            # Count
            item = QtGui.QTableWidgetItem()
            item.setText(tblOutcome.getField('count'))
            item.setTextAlignment(QtCore.Qt.AlignRight)
            self.table.setItem(rec, 2, item)
            # Count
            item = QtGui.QTableWidgetItem()
            item.setText(tblOutcome.getField('price'))
            item.setTextAlignment(QtCore.Qt.AlignRight)
            self.table.setItem(rec, 3, item)
            #, tblIncome.getField('reference')
            tblOutcome.Next()


    def show_income_table(self):
        self.view_mode('income')
        # Marking main table
        columns = (
        ("ID", 0),
        ("PROV_ID", 0),
        ("ITEM_PROV", 150),
        ("PROD_GROUP_ID", 0),
        ("PROD_GROUP", 150),
        ("PROD_NAME_ID", 0),
        ("PROD_NAME", 150),
        ("ITEM_COUNT", 100),
        ("ITEM_PRICE", 100),
        ("ITEM_TOTAL_PRICE", 100),
        ("PROD_NDS", 100))
        self.table.clear()
        self.table.setColumnCount(len(columns))
        self.table.setRowCount(0)
        for c in range(len(columns)):
            headerItem0 = QtGui.QTableWidgetItem()
            headerItem0.setText(insmes(columns[c][0]))
            self.table.setHorizontalHeaderItem(c, headerItem0)
            self.table.horizontalHeader().resizeSection(c, columns[c][1])
        # filling table
        income = mdata.get_income(self.get_cur_date())
        for l in range(len(income)):
        # add line
            self.table.insertRow(self.table.rowCount())
            # insert income ID
            item = QtGui.QTableWidgetItem()
            item.setText(str(income[l]['id']))
            self.table.setItem(l, 0, item)
            # insert provider ID
            del(item)
            item = QtGui.QTableWidgetItem()
            item.setText(str(income[l]['provider']))
            self.table.setItem(l, 1, item)
            # insert provider name
            del(item)
            item = QtGui.QTableWidgetItem()
            item.setText(mdata.get_provider_short_name(income[l]['provider']))
            self.table.setItem(l, 2, item)
            # insert product group id
            #del(item) 
            #item = QtGui.QTableWidgetItem()
            #item.setText(str(income[l]['product_group']))
            #self.table.setItem(l, 3, item)
            # insert product group
            del(item)
            item = QtGui.QTableWidgetItem()
            item.setText(mdata.prodgr_by_product(income[l]['product']))
            self.table.setItem(l, 4, item)
            #insert product ID
            del(item)
            item = QtGui.QTableWidgetItem()
            item.setText(str(income[l]['product']))
            self.table.setItem(l, 5, item)
            # insert product
            del(item)
            item = QtGui.QTableWidgetItem()
            item.setText(mdata.get_product_name(income[l]['product']))
            self.table.setItem(l, 6, item)
            # insert product count
            del(item)
            item = QtGui.QTableWidgetItem()
            item.setText(str(income[l]['count']))
            self.table.setItem(l, 7, item)
            # insert product price
            del(item)
            item = QtGui.QTableWidgetItem()
            item.setText(str(income[l]['price']))
            self.table.setItem(l, 8, item)
            # insert product summa
            del(item)
            item = QtGui.QTableWidgetItem()
            summa = int(income[l]['count']) * float(income[l]['price'])
            item.setText(str(summa))
            self.table.setItem(l, 9, item)
            # insert product nds
            del(item)
            item = QtGui.QTableWidgetItem()
            item.setText(str(income[l]['nds']))
            self.table.setItem(l, 10, item)


    def show_balance_table(self):
        globals()['cur_view'] = 'balance'
        # Marking main table
        self.table.clear()
        self.table.setColumnCount(3)
        self.table.setRowCount(0)
        headerItem1 = QtGui.QTableWidgetItem()
        headerItem1.setText(insmes('ITEM_NAME'))
        headerItem2 = QtGui.QTableWidgetItem()
        headerItem2.setText(insmes('ITEM_COUNT'))
        headerItem3 = QtGui.QTableWidgetItem()
        headerItem3.setText(insmes('ITEM_PRICE'))
        self.table.setHorizontalHeaderItem(0, headerItem1)
        self.table.horizontalHeader().resizeSection(0, 400)
        self.table.setHorizontalHeaderItem(1, headerItem2)
        self.table.horizontalHeader().resizeSection(1, 100)
        self.table.setHorizontalHeaderItem(2, headerItem3)
        self.table.horizontalHeader().resizeSection(2, 100)
        tblRefer = csvtable.csvtable()
        tblIncome = csvtable.csvtable()
        tblOutcome = csvtable.csvtable()
        tblRefer.LoadFromFile('reference.csv')
        tblIncome.LoadFromFile('income.csv')
        tblOutcome.LoadFromFile('outcome.csv')


        #filling table
        bal = balance.balance(tblIncome.getTable(), tblOutcome.getTable())


        #print bal

        #exit()
        self.table.setRowCount(len(bal))
        rec = 0
        for b in bal:
        # insert refer name
            item = QtGui.QTableWidgetItem()
            item.setText(self.get_refer_name(str(b[0])))
            self.table.setItem(rec, 0, item)

            item = QtGui.QTableWidgetItem()
            item.setText(str(b[2]))
            item.setTextAlignment(QtCore.Qt.AlignRight)
            self.table.setItem(rec, 1, item)

            item = QtGui.QTableWidgetItem()
            item.setText(str(b[3]))
            item.setTextAlignment(QtCore.Qt.AlignRight)
            self.table.setItem(rec, 2, item)

            rec += 1


    def show_item_edit(self):
        if self.view_mode() == 'directories':
            cur_row = self.table.currentRow()
            if self.get_cell_value(cur_row, 0) == 'TITLE_PROV':
                self.show_providers_table()
            elif self.get_cell_value(cur_row, 0) == 'TITLE_GPROD':
                self.show_products_groups_table()
            elif self.get_cell_value(cur_row, 0) == 'TITLE_PROD':
                self.show_products_table()
            elif self.get_cell_value(cur_row, 0) == 'TITLE_RCPT':
                self.show_recipients_table()

        elif self.view_mode() == 'providers':
            cur_row = self.table.currentRow()
            provedit.Ui_dialog.title = self.get_cell_value(cur_row, 1)
            provedit.Ui_dialog.labelShortName = self.get_cell_value(cur_row, 1)
            provedit.Ui_dialog.labelFullName = self.get_cell_value(cur_row, 2)
            provedit.Ui_dialog.labelAddress = self.get_cell_value(cur_row, 3)
            provedit.Ui_dialog.labelInn = self.get_cell_value(cur_row, 4)
            provedit.Ui_dialog.labelKpp = self.get_cell_value(cur_row, 5)
            provedit.Ui_dialog.labelAcc = self.get_cell_value(cur_row, 6)
            provedit.Ui_dialog.labelBnk = self.get_cell_value(cur_row, 7)

            dialog = provedit.Ui_dialog()
            result = dialog.exec_()
            if result:
            # Update db
                q = """
                short_name = '%s',
                full_name = '%s',
                address = '%s',
                inn = '%s',
                kpp = '%s',
                account = '%s',
                bank = '%s' """ % (
                dialog.editShortName.text(),
                dialog.editFullName.text(),
                dialog.editAddress.text(),
                dialog.editInn.text(),
                dialog.editKpp.text(),
                dialog.editAcc.text(),
                dialog.editBank.text()
                )
                mdata.upd_provider(int(self.get_cell_value(cur_row, 0)), q)
                #update table
                self.show_providers_table()
        elif self.view_mode() == 'products_groups':
            cur_row = self.table.currentRow()
            lineedit.Ui_dialog.title = self.get_cell_value(cur_row, 1)
            lineedit.Ui_dialog.text = self.get_cell_value(cur_row, 1)
            dialog = lineedit.Ui_dialog()
            result = dialog.exec_()
            if result:
            # Update db
                mdata.upd_products_groups(int(self.get_cell_value(cur_row, 0)), dialog.line.text())
                #update table
                self.show_products_groups_table()
        elif self.view_mode() == 'products':
            cur_row = self.table.currentRow()
            prgr4out = []
            for r in mdata.get_products_groups():
                prgr4out.append((int(r['id']), r['name']))
            prodedit.dialog_window.product_group = prgr4out
            prodedit.dialog_window.cur_product_group = self.get_cell_value(cur_row, 1)
            prodedit.dialog_window.product_name = self.get_cell_value(cur_row, 3)
            prodedit.dialog_window.product_unit = self.get_cell_value(cur_row, 2)
            dialog = prodedit.dialog_window()
            result = dialog.exec_()
            if result:
                mdata.upd_products(int(self.get_cell_value(cur_row, 0)),
                                   dialog.cur_product_group, dialog.product_name, dialog.product_unit)
                self.show_products_table()


    def keyPressEvent(self, event):
    #if event.key() == QtCore.Qt.Key_Escape:
    #    self.close()

    #if event.key() == QtCore.Qt.Key_Insert:
    #     self.table.insertRow(self.table.currentRow())

    #if event.key() == QtCore.Qt.Key_Down:
    #    self.table.setRowCount(self.table.rowCount() + 1)
    #    self.table.setFocus()

        if event.key() == QtCore.Qt.Key_Delete:
            if str(self.table.rowCount()) != str(0):
                reply = QtGui.QMessageBox.question(self, 'Message',
                                                   "Are you sure to delete?", QtGui.QMessageBox.Yes,
                                                   QtGui.QMessageBox.No)

                if reply == QtGui.QMessageBox.Yes:
                    cur_row = self.table.currentRow()
                    cur_col = self.table.currentColumn()
                    self.table.removeRow(cur_row)
                    self.table.setFocus()
                    self.table.currentRow

        if event.key() == QtCore.Qt.Key_Escape:
            self.close()
        #reply = QtGui.QMessageBox.question(self, 'Message',
        #"Are you sure to quit?", QtGui.QMessageBox.Yes, QtGui.QMessageBox.No)

        #if reply == QtGui.QMessageBox.Yes:
        #    event.accept()
        #else:
        #    event.ignore()

    def txt_out(self):
        if globals()['cur_view'] == 'income':
            tblRefer = csvtable.csvtable()
            tblProv = csvtable.csvtable()
            tblIncome = csvtable.csvtable()
            tblRefer.LoadFromFile('reference.csv')
            tblProv.LoadFromFile('providers.csv')
            tblIncome.LoadFromFile('income.csv')
            #Filter by cur_date 
            #tblIncome.Filter(True, 'date', self.get_cur_date())
            out = []
            tblIncome.First()
            for rec in range(0, tblIncome.RecCount()):
                l = []
                l.append(self.get_prov_name(tblIncome.getField('provider')))
                l.append(self.get_refer_name(tblIncome.getField('reference')))
                l.append(tblIncome.getField('count'))
                l.append(tblIncome.getField('price'))
                out.append(l)
                tblIncome.Next()
            out_date = self.get_cur_date()
            out_date = out_date[6:8] + '.' + out_date[4:6] + '.' + out_date[:4]
            txtout.income_table(out, out_date)
        if globals()['cur_view'] == 'balance':
            tblRefer = csvtable.csvtable()
            tblIncome = csvtable.csvtable()
            tblOutcome = csvtable.csvtable()
            tblRefer.LoadFromFile('reference.csv')
            tblIncome.LoadFromFile('income.csv')
            tblOutcome.LoadFromFile('outcome.csv')
            #filling table
            bal = balance.balance(tblIncome.getTable(), tblOutcome.getTable())
            self.table.setRowCount(len(bal))
            out = []
            for b in bal:
                l = []
                l.append(self.get_refer_name(str(b[0])))
                l.append(str(b[1]))
                l.append(str(b[2]))
                out.append(l)
                tblIncome.Next()
            out_date = self.get_cur_date()
            out_date = out_date[6:8] + '.' + out_date[4:6] + '.' + out_date[:4]
            txtout.balance_table(out, out_date)


if __name__ == "__main__":
    app = QtGui.QApplication(sys.argv)
    #f1 = itemedit.item_edit_window()
    app.setStyle("Cleanlooks")
    window = MainWindow()
    #window.setWindowState(QtCore.Qt.WindowMaximized)
    window.resize(640, 600)
    window.set_cur_date()
    window.show()
    sys.exit(app.exec_())     
    
