from PyQt4 import QtCore, QtGui
import messages
import mdata


def insmes(txt):
    return unicode(messages.messages[txt], "cp1251")

class dialog_window(QtGui.QDialog):
    title = " "
    cur_product_group = 2
    cur_product = 1
    nds = 18
    count = 0
    price = 0

    
    def __init__(self):
        QtGui.QDialog.__init__(self)
        self.setupUi()
   
    def setupUi(self):

        self.setObjectName("dialog")
        self.resize(QtCore.QSize(QtCore.QRect(0,0,400,220).size()).expandedTo(self.minimumSizeHint()))
        self.setWindowTitle(self.title)
        
        self.buttonBox = QtGui.QDialogButtonBox(self)
        self.buttonBox.setGeometry(QtCore.QRect(50,180,341,32))
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtGui.QDialogButtonBox.Cancel|QtGui.QDialogButtonBox.NoButton|QtGui.QDialogButtonBox.Ok)
        
        self.label0 = QtGui.QLabel(self)
        self.label0.setGeometry(QtCore.QRect(10,0,211,16))
        self.label0.setText(insmes("PROD_GROUP"))
        self.PRGR_comboBox = QtGui.QComboBox(self)
        self.PRGR_comboBox.setGeometry(QtCore.QRect(10,20,381,22))
        i = -1
        for r in mdata.get_products_groups():
            if len(mdata.get_products_by_group(r['id'])) != 0:
                i += 1
                self.PRGR_comboBox.addItem(r['name'])
                if r['id'] == self.cur_product_group:
                    self.PRGR_comboBox.setCurrentIndex(i)

        self.label = QtGui.QLabel(self)
        self.label.setGeometry(QtCore.QRect(10,50,211,16))
        self.label.setText(insmes("PROD_NAME"))
        self.PROD_comboBox = QtGui.QComboBox(self)
        self.PROD_comboBox.setGeometry(QtCore.QRect(10,70,381,22))
                
        self.label1 = QtGui.QLabel(self)
        self.label1.setGeometry(QtCore.QRect(10,100,211,22))
        self.label1.setText(insmes("PROD_UNIT"))
        self.labelUnit = QtGui.QLabel(self)
        self.labelUnit.setGeometry(QtCore.QRect(60,100,211,22))
        self.labelUnit.setText('')
        self.label11 = QtGui.QLabel(self)
        self.label11.setGeometry(QtCore.QRect(150,100,211,22))
        self.label11.setText(insmes("PROD_NDS"))
        self.NDS_edit = QtGui.QDoubleSpinBox(self)
        self.NDS_edit.setGeometry(QtCore.QRect(200,100,70,22))
        self.NDS_edit.setValue(self.nds)
        self.NDS_edit.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.NDS_edit.setMaximum(99.99)
        
        self.label2 = QtGui.QLabel(self)
        self.label2.setGeometry(QtCore.QRect(10,130,150,22))
        self.label2.setText(insmes('ITEM_COUNT'))
        self.COUNT_edit = QtGui.QSpinBox(self)
        self.COUNT_edit.setGeometry(QtCore.QRect(10,150,180,22))
        self.COUNT_edit.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.COUNT_edit.setMaximum(999999999)
        self.COUNT_edit.setValue(self.count)
        
        self.label22 = QtGui.QLabel(self)
        self.label22.setGeometry(QtCore.QRect(200,130,150,22))
        self.label22.setText(insmes('ITEM_PRICE'))
        self.PRICE_edit = QtGui.QDoubleSpinBox(self)
        self.PRICE_edit.setGeometry(QtCore.QRect(200,150,190,22))
        self.PRICE_edit.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.PRICE_edit.setMaximum(999999999.99)
        self.PRICE_edit.setValue(self.price)
        
        QtCore.QObject.connect(self.buttonBox,QtCore.SIGNAL("accepted()"),self.accept)
        QtCore.QObject.connect(self.buttonBox,QtCore.SIGNAL("rejected()"),self.reject)
        QtCore.QObject.connect(self.PRGR_comboBox,QtCore.SIGNAL("currentIndexChanged(int)"),self.setPRGR)
        QtCore.QObject.connect(self.PROD_comboBox,QtCore.SIGNAL("currentIndexChanged(int)"),self.setPROD)
        QtCore.QObject.connect(self.NDS_edit,QtCore.SIGNAL("valueChanged(double)"),self.setNDS)
        QtCore.QObject.connect(self.COUNT_edit,QtCore.SIGNAL("valueChanged(int)"),self.setCOUNT)
        QtCore.QObject.connect(self.PRICE_edit,QtCore.SIGNAL("valueChanged(double)"),self.setPRICE)

        QtCore.QMetaObject.connectSlotsByName(self)
        self.setPRGR()
        self.setPROD()


    def getGroupID(self, index):
        j = -1
        for i in mdata.get_products_groups():
            if len(mdata.get_products_by_group(i['id'])) != 0:
                j += 1
                if index == j: return i['id']

    def getProdID(self, index):
        j = -1
        for i in mdata.get_products_by_group(self.cur_product_group):
            j += 1
            if index == j: return i['id']
    def setNDS(self):
        self.nds = self.NDS_edit.value()
    def setCOUNT(self):
        self.count = self.COUNT_edit.value()
    def setPRICE(self):
        self.price = self.PRICE_edit.value()
        
    def setPRGR(self):
        self.cur_product_group = self.getGroupID(self.PRGR_comboBox.currentIndex())
        self.PROD_comboBox.clear()
        for r in mdata.get_products_by_group(self.cur_product_group):
            self.PROD_comboBox.addItem(r['name'])        
        
    def setPROD(self):
        self.cur_product = self.getProdID(self.PROD_comboBox.currentIndex())
        #self.labelUnit.setText()
        ind = self.getProdID(self.PROD_comboBox.currentIndex())
        if ind != None:
            for r in  mdata.get_products_by_id(self.getProdID(self.PROD_comboBox.currentIndex())):
                self.labelUnit.setText(r['unit'])
                self.setWindowTitle(r['name'])
        
if __name__ == '__main__':
    app = QtGui.QApplication([])
    dialog = dialog_window()
    result = dialog.exec_()
    #if result: print 'OK Button pressed'
    #else: print 'Cancel Button pressed'