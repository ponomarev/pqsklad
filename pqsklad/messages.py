# -*- coding: cp1251 -*-

messages = {
"ITEM_NAME": "������������",
"ITEM_PROV": "���������",
"ITEM_RCPT": "����������",
"ITEM_COUNT": "����������",
"ITEM_PRICE": "����",
"ITEM_TOTAL_PRICE": "�����",
"TITLE_REFERENCE": "�����",
"TITLE_MN_WN": "�����",

"TITLE_DIRS": "�����������",
"TITLE_DIR": "����������",
"TITLE_PROV": "���������� �����������",
"TITLE_RCPT": "���������� �����������",
"TITLE_PROD": "���������� �������",
"TITLE_GPROD": "���������� ����� �������",
"TITLE_GPROD_NAME": "������������ ������ �������",

"TITLE_INC": "�����������",
"TITLE_BAL": "�����",
"TITLE_OUT": "��������",
#"TITLE_REF": "���������� �������",
"ID": "ID",

"TITLE_INCOME": "������",
"TITLE_OUTCOME": "������",
"TITLE_TXTOUT": "TXT",
"TITLE_CSVOUT": "CSV",

"PROV_SH_NAME": "������� ��������",
"PROV_FL_NAME": "������ ��������",
"PROV_ADDRESS": "�����",
"PROV_INN": "���",
"PROV_ID": "ID ����������",
"PROV_KPP": "���",
"PROV_ACC": "����",
"PROV_BNK": "����",

"RCPT_SH_NAME": "������� ��������",
"RCPT_FL_NAME": "������ ��������",
"RCPT_ADDRESS": "�����",
"RCPT_INN": "���",
"RCPT_ID": "ID ����������",
"RCPT_KPP": "���",
"RCPT_ACC": "����",
"RCPT_BNK": "����",

"PROD_GROUP": "������ �������",
"PROD_GROUP_ID": "ID ������ �������",
"PROD_UNIT": "��. ���.",
"PROD_NAME": "������������",
"PROD_NAME_ID": "ID ������������",
"PROD_NDS": "���",
"QMESS_DEL": "�����?",
"PRINT": "������"

}

def get(txt):
    return unicode(messages[txt], "cp1251")


if __name__ == '__main__':
    print messages
