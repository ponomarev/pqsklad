from PyQt4 import QtCore, QtGui
import messages

def insmes(txt):
    return unicode(messages.messages[txt], "cp1251")

class Ui_dialog(QtGui.QDialog):
    title = " "
    labelShortName = ""
    labelFullName = ""
    labelAddress = ""
    labelInn = ""
    labelKpp = ""
    labelAcc = ""
    labelBnk = ""

    def __init__(self):
        QtGui.QDialog.__init__(self)
        self.setupUi()
        
    def setupUi(self):
       
        self.setObjectName("dialog")
        self.resize(QtCore.QSize(QtCore.QRect(0, 0, 380, 200).size()).expandedTo(self.minimumSizeHint()))
        self.setWindowTitle(self.title)
        
        self.label0 = QtGui.QLabel(self)
        self.label0.setGeometry(QtCore.QRect(10, 10, 100, 20))
        self.label0.setText(insmes('PROV_SH_NAME'))
        self.editShortName = QtGui.QLineEdit(self)
        self.editShortName.setGeometry(QtCore.QRect(120, 10, 250, 20))
        self.editShortName.setText(self.labelShortName)
        
        self.label0 = QtGui.QLabel(self)
        self.label0.setGeometry(QtCore.QRect(10, 30, 211, 20))
        self.label0.setText(insmes('PROV_FL_NAME'))
        self.editFullName = QtGui.QLineEdit(self)
        self.editFullName.setGeometry(QtCore.QRect(120, 30, 250, 20))
        self.editFullName.setText(self.labelFullName)

        self.label0 = QtGui.QLabel(self)
        self.label0.setGeometry(QtCore.QRect(10,50,211,16))
        self.label0.setText(insmes('PROV_ADDRESS'))
        self.editAddress = QtGui.QLineEdit(self)
        self.editAddress.setGeometry(QtCore.QRect(120, 50, 250, 20))
        self.editAddress.setText(self.labelAddress)

        self.label0 = QtGui.QLabel(self)
        self.label0.setGeometry(QtCore.QRect(10, 70, 211, 20))
        self.label0.setText(insmes('PROV_INN'))
        self.editInn = QtGui.QLineEdit(self)
        self.editInn.setGeometry(QtCore.QRect(120, 70, 250, 20))
        self.editInn.setText(self.labelInn)

        self.label0 = QtGui.QLabel(self)
        self.label0.setGeometry(QtCore.QRect(10, 90, 211, 20))
        self.label0.setText(insmes('PROV_KPP'))
        self.editKpp = QtGui.QLineEdit(self)
        self.editKpp.setGeometry(QtCore.QRect(120, 90, 250, 20))
        self.editKpp.setText(self.labelKpp)

        self.label0 = QtGui.QLabel(self)
        self.label0.setGeometry(QtCore.QRect(10,110,211, 20))
        self.label0.setText(insmes('PROV_ACC'))
        self.editAcc = QtGui.QLineEdit(self)
        self.editAcc.setGeometry(QtCore.QRect(120, 110, 250, 20))
        self.editAcc.setText(self.labelAcc)

        self.label0 = QtGui.QLabel(self)
        self.label0.setGeometry(QtCore.QRect(10,130,211, 20))
        self.label0.setText(insmes('PROV_BNK'))
        self.editBank = QtGui.QLineEdit(self)
        self.editBank.setGeometry(QtCore.QRect(120, 130, 250, 20))
        self.editBank.setText(self.labelBnk)

        self.buttonBox = QtGui.QDialogButtonBox(self)
        self.buttonBox.setGeometry(QtCore.QRect(170, 160, 200, 32))
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtGui.QDialogButtonBox.Cancel|QtGui.QDialogButtonBox.NoButton|QtGui.QDialogButtonBox.Ok)
        self.buttonBox.setObjectName("buttonBox")
        
        QtCore.QObject.connect(self.buttonBox,QtCore.SIGNAL("accepted()"),self.accept)
        QtCore.QObject.connect(self.buttonBox,QtCore.SIGNAL("rejected()"),self.reject)
        QtCore.QMetaObject.connectSlotsByName(self)
        
if __name__ == '__main__':
    app = QtGui.QApplication([])
    app.setStyle("Cleanlooks")
    dialog = Ui_dialog()
    result = dialog.exec_()
    if result:
        print 'Ok Button pressed'
    else: 
        print 'Cancel Button pressed'
    