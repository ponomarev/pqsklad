from PyQt4 import QtCore, QtGui
import csvtable

reference = []
key = ''

class Ui_dialog(QtGui.QDialog):
    label_name = "Name"
    tblRefer = csvtable.csvtable()

    def __init__(self):
        QtGui.QDialog.__init__(self)
        self.setupUi()
        
    def setupUi(self):
        self.tblRefer.setTable(globals()['reference'])
        self.tblRefer.setKey(globals()['key'])
        
        self.setObjectName("dialog")
        self.resize(QtCore.QSize(QtCore.QRect(0,0,500,500).size()).expandedTo(self.minimumSizeHint()))

        self.refer_table = QtGui.QTableWidget(self)
        self.refer_table.setGeometry(QtCore.QRect(5,5,490,450))
        self.refer_table.clear()
        self.refer_table.setColumnCount(1)
        headerItem = QtGui.QTableWidgetItem()
        headerItem.setText(unicode(self.label_name, "cp1251")) 
        self.refer_table.setHorizontalHeaderItem(0,headerItem)
        self.refer_table.setAlternatingRowColors(True)
        self.refer_table.horizontalHeader().resizeSection(0, 470)
        
        self.refer_table.setColumnCount(1)
        self.refer_table.setRowCount(self.tblRefer.RecCount())
        self.tblRefer.First()
        for row in range(0, self.tblRefer.RecCount()):
            item = QtGui.QTableWidgetItem()
            item.setText(self.tblRefer.getField('name'))
            self.refer_table.setItem(row, 0, item)
            self.tblRefer.Next()  
            
        self.AddBotton = QtGui.QPushButton(self)
        self.AddBotton.setGeometry(QtCore.QRect(10,465,30,30))
        self.AddBotton.setText('+')
        self.AddBotton.setToolTip('Ctrl+Insert')

        self.DelBotton = QtGui.QPushButton(self)
        self.DelBotton.setGeometry(QtCore.QRect(50,465,30,30))
        self.DelBotton.setText('-')
        self.DelBotton.setToolTip('Ctrl+Delete')

        self.buttonBox = QtGui.QDialogButtonBox(self)
        self.buttonBox.setGeometry(QtCore.QRect(130,465,341,32))
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtGui.QDialogButtonBox.Cancel|QtGui.QDialogButtonBox.NoButton|QtGui.QDialogButtonBox.Ok)
        self.buttonBox.setObjectName("buttonBox")
        
        QtCore.QObject.connect(self.buttonBox,QtCore.SIGNAL("accepted()"),self.accept)
        QtCore.QObject.connect(self.buttonBox,QtCore.SIGNAL("rejected()"),self.reject)
        QtCore.QObject.connect(self.refer_table,QtCore.SIGNAL("cellChanged(int,int)"),self.setItems)

        self.connect(self.AddBotton, QtCore.SIGNAL("clicked()"), self.addItem)
        self.AddBotton.setShortcut('Ctrl+Insert')
        self.connect(self.DelBotton, QtCore.SIGNAL("clicked()"), self.delItem)
        self.DelBotton.setShortcut('Ctrl+Delete')
        QtCore.QMetaObject.connectSlotsByName(self)

    def addItem(self):
        self.refer_table.setRowCount(self.refer_table.rowCount() + 1)
        self.tblRefer.Insert()
        globals()['reference'] = self.tblRefer.getTable()

    def delItem(self):
        cur_row = self.refer_table.currentRow()
        self.tblRefer.index = cur_row
        self.tblRefer.Delete()
        self.refer_table.removeRow(cur_row)
        globals()['reference'] = self.tblRefer.getTable()
        
    def setItems(self):
        cur_row = self.refer_table.currentRow()
        self.tblRefer.index = cur_row
        item = QtGui.QTableWidgetItem()
        item = self.refer_table.item(cur_row, 0)
        self.tblRefer.setField('name', str(item.text()))
        globals()['reference'] = self.tblRefer.getTable()
        
if __name__ == '__main__':
    tbl = csvtable.csvtable()
    tbl.LoadFromFile('reference.csv')
    globals()['reference'] = tbl.getTable()
    globals()['key'] = 'id'

    app = QtGui.QApplication([])
    dialog = Ui_dialog()
    result = dialog.exec_()
    if result:
        print globals()['reference']
        tbl.setTable(globals()['reference'])
        tbl.Print()
        tbl.SaveToFile('reference.csv')
        
#    else: 
#        print 'Cancel Button pressed'
    