__doc__ = """
This form manage and adding outcoming products
for each recipient.
"""
__author__ = "Sergey Ponomarev <ponomarevsa@yandex.ru>"

from PyQt4 import QtCore, QtGui
import messages
import mdata
import outcomeadd

class dialog_window(QtGui.QDialog):
    title = " "
    date = "20081227"
    cur_prov = 0
    providers = []
    
    def __init__(self):
        QtGui.QDialog.__init__(self)
        self.setupUi()
   
    def setupUi(self):
        self.setObjectName("dialog")
        self.resize(QtCore.QSize(QtCore.QRect(0,0,800,600).size()).expandedTo(self.minimumSizeHint()))
        self.setWindowTitle(self.title)
        
        self.buttonBox = QtGui.QDialogButtonBox(self)
        self.buttonBox.setGeometry(QtCore.QRect(450,560,341,32))
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtGui.QDialogButtonBox.Cancel|QtGui.QDialogButtonBox.NoButton|QtGui.QDialogButtonBox.Ok)
        
        self.label0 = QtGui.QLabel(self)
        self.label0.setGeometry(QtCore.QRect(10,0,211,16))
        self.label0.setText(messages.get("ITEM_RCPT"))
        self.PROV_comboBox = QtGui.QComboBox(self)
        self.PROV_comboBox.setGeometry(QtCore.QRect(10,20,300,22))
        self.providers = []
        i = -1
        for r in mdata.get_recipients():
            i += 1
            self.providers.append([i, r['id']])
            self.PROV_comboBox.addItem(r['short_name'])

        btnAdd = QtGui.QPushButton(self)
        btnAdd.setGeometry(QtCore.QRect(720,15,32,32))
        btnAdd.setText("+")        
        btnAdd.setToolTip('Ctrl+Insert')

        btnDel = QtGui.QPushButton(self)
        btnDel.setGeometry(QtCore.QRect(760,15,32,32))
        btnDel.setText("-")        
        btnDel.setToolTip('Ctrl+Delete')
        
        self.label1 = QtGui.QLabel(self)
        self.label1.setGeometry(QtCore.QRect(10,50,211,16))
        self.label1.setText(messages.get("TITLE_OUTCOME"))
        
        self.outcome_table = QtGui.QTableWidget(self)
        self.outcome_table.setGeometry(QtCore.QRect(10,70,780,480))
        self.outcome_table.setAlternatingRowColors(True)
        self.outcome_table.setEditTriggers(QtGui.QAbstractItemView.NoEditTriggers)
        
        
        QtCore.QObject.connect(self.buttonBox,QtCore.SIGNAL("accepted()"),self.save_outcome)
        QtCore.QObject.connect(self.buttonBox,QtCore.SIGNAL("rejected()"),self.reject)
        self.connect(self.PROV_comboBox, QtCore.SIGNAL("currentIndexChanged(int)"), self.set_recipient)
        self.connect(btnAdd, QtCore.SIGNAL("clicked()"), self.add_item)
        btnAdd.setShortcut('Ctrl+Insert')
        self.connect(btnDel, QtCore.SIGNAL("clicked()"), self.del_item)
        btnDel.setShortcut('Ctrl+Delete')

        QtCore.QMetaObject.connectSlotsByName(self)
        self.marking_table()
        self.set_recipient()
    
    def save_outcome(self):
        for row in xrange(self.outcome_table.rowCount()):
            item = QtGui.QTableWidgetItem()
            item = self.outcome_table.item(row, 2)
            product = item.text()
            item = self.outcome_table.item(row, 5)
            count = item.text()
            item = self.outcome_table.item(row, 6)
            price = item.text()
            item = self.outcome_table.item(row, 8)
            nds = item.text()
            print nds
            mdata.add_outcome(product, self.cur_prov, self.date, int(count), float(price), float(nds))
        self.accept()
    
    def add_item(self):
        dialog = outcomeadd.dialog_window()
        result = dialog.exec_()
        if result:
            row_count = self.outcome_table.rowCount()
            self.outcome_table.setRowCount(row_count + 1)
            # add prod group ID 
            item = QtGui.QTableWidgetItem()
            item.setText(str(dialog.cur_product_group))
            self.outcome_table.setItem(row_count, 0, item)
            # add prod group 
            item = QtGui.QTableWidgetItem()
            item.setText(mdata.get_products_groups_by_id(dialog.cur_product_group))
            self.outcome_table.setItem(row_count, 1, item)
            # add name prod ID
            item = QtGui.QTableWidgetItem()
            item.setText(str(dialog.cur_product))
            self.outcome_table.setItem(row_count, 2, item)
            # add name prod
            item = QtGui.QTableWidgetItem()
            item.setText(mdata.get_product_name_by_id(dialog.cur_product))
            self.outcome_table.setItem(row_count, 3, item)
            # add unit prod
            item = QtGui.QTableWidgetItem()
            item.setText(mdata.get_product_unit_by_id(dialog.cur_product))
            self.outcome_table.setItem(row_count, 4, item)
            # add count
            item = QtGui.QTableWidgetItem()
            item.setText(str(dialog.count))
            self.outcome_table.setItem(row_count, 5, item)
            # add price
            item = QtGui.QTableWidgetItem()
            item.setText(str(dialog.price))
            self.outcome_table.setItem(row_count, 6, item)
            # add summa
            item = QtGui.QTableWidgetItem()
            summa = dialog.price* dialog.count
            item.setText(str(summa))
            self.outcome_table.setItem(row_count, 7, item)
            # add NDS
            item = QtGui.QTableWidgetItem()
            item.setText(str(dialog.nds))
            self.outcome_table.setItem(row_count, 8, item)


    def del_item(self):
        cur_row = self.outcome_table.currentRow()
        if cur_row < 0:
            return
        reply = QtGui.QMessageBox.question(self, '' ,
                    messages.get("QMESS_DEL"), QtGui.QMessageBox.Yes, QtGui.QMessageBox.No)
        if reply == QtGui.QMessageBox.Yes:
            self.outcome_table.removeRow(cur_row)


    def marking_table(self):
        columns = (
        ("PROD_GROUP_ID", 0),
        ("PROD_GROUP", 200),
        ("PROD_NAME_ID", 0),
        ("PROD_NAME", 200),
        ("PROD_UNIT", 50),  
        ("ITEM_COUNT", 70),
        ("ITEM_PRICE", 100),
        ("ITEM_TOTAL_PRICE", 100),
        ("PROD_NDS", 50)
        )
        self.outcome_table.clear()
        self.outcome_table.setColumnCount(len(columns))
        self.outcome_table.setRowCount(0)
        for c in range(len(columns)):
            headerItem0 = QtGui.QTableWidgetItem()
            headerItem0.setText(messages.get(columns[c][0]))
            self.outcome_table.setHorizontalHeaderItem(c,headerItem0)
            self.outcome_table.horizontalHeader().resizeSection(c, columns[c][1])
    
    def set_recipient(self):
        #self.cur_product_group = self.product_group[self.PRGR_comboBox.currentIndex()][0]
        self.setWindowTitle(messages.get("TITLE_OUTCOME") + " - " + self.PROV_comboBox.currentText())
        for i in self.providers:
            if i[0] == self.PROV_comboBox.currentIndex():
                self.cur_prov = i[1] 
         
    def setPROD(self):
        self.product_name = self.PROD_edit.text()
        self.setWindowTitle(self.product_name)

    def setPRUT(self):
        self.product_unit = self.PRUT_edit.text()

        
