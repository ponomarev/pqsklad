from PyQt4 import QtCore, QtGui
import messages


def insmes(txt):
    return unicode(messages.messages[txt], "cp1251")

class dialog_window(QtGui.QDialog):
    title = " "
    #text = ""
    product_group = ((10, 'asdasd'), (22, 'aaggg2'), (3333, 'aaggg3'))
    cur_product_group = 22
    product_name = "1"
    product_unit = "as"
    
    def __init__(self):
        QtGui.QDialog.__init__(self)
        self.setupUi()
   
    def setupUi(self):
        #self.tblProv.setTable(globals()['providers'])
        #self.tblRefer.setTable(globals()['reference'])

        self.setObjectName("dialog")
        self.resize(QtCore.QSize(QtCore.QRect(0,0,400,200).size()).expandedTo(self.minimumSizeHint()))
        self.setWindowTitle(self.title)
        
        self.buttonBox = QtGui.QDialogButtonBox(self)
        self.buttonBox.setGeometry(QtCore.QRect(50,165,341,32))
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtGui.QDialogButtonBox.Cancel|QtGui.QDialogButtonBox.NoButton|QtGui.QDialogButtonBox.Ok)
        
        self.label0 = QtGui.QLabel(self)
        self.label0.setGeometry(QtCore.QRect(10,0,211,16))
        self.label0.setText(insmes("PROD_GROUP"))
        self.PRGR_comboBox = QtGui.QComboBox(self)
        self.PRGR_comboBox.setGeometry(QtCore.QRect(10,20,381,22))
        i = -1
        for l in self.product_group:
            i += 1
            self.PRGR_comboBox.addItem(l[1])
            if l[0] == self.cur_product_group:
                self.PRGR_comboBox.setCurrentIndex(i)

        self.label = QtGui.QLabel(self)
        self.label.setGeometry(QtCore.QRect(10,50,211,16))
        self.label.setText(insmes("PROD_NAME"))
        self.PROD_edit = QtGui.QLineEdit(self)
        self.PROD_edit.setGeometry(QtCore.QRect(10,70,381,22))
        self.PROD_edit.setText(self.product_name)
                
        self.label1 = QtGui.QLabel(self)
        self.label1.setGeometry(QtCore.QRect(10,100,211,16))
        self.label1.setText(insmes("PROD_UNIT"))
        self.PRUT_edit = QtGui.QLineEdit(self)
        self.PRUT_edit.setGeometry(QtCore.QRect(10,120,381,22))
        self.PRUT_edit.setText(self.product_unit)        
        
        QtCore.QObject.connect(self.buttonBox,QtCore.SIGNAL("accepted()"),self.accept)
        QtCore.QObject.connect(self.buttonBox,QtCore.SIGNAL("rejected()"),self.reject)
        QtCore.QObject.connect(self.PRGR_comboBox,QtCore.SIGNAL("currentIndexChanged(int)"),self.setPRGR)
        QtCore.QObject.connect(self.PROD_edit,QtCore.SIGNAL("textEdited(QString)"),self.setPROD)
        QtCore.QObject.connect(self.PRUT_edit,QtCore.SIGNAL("textEdited(QString)"),self.setPRUT)
        QtCore.QMetaObject.connectSlotsByName(self)
        self.cur_product_group = self.product_group[self.PRGR_comboBox.currentIndex()][0]

    def setPRGR(self):
        self.cur_product_group = self.product_group[self.PRGR_comboBox.currentIndex()][0]
    def setPROD(self):
        self.product_name = self.PROD_edit.text()
        self.setWindowTitle(self.product_name)
    def setPRUT(self):
        self.product_unit = self.PRUT_edit.text()

        
if __name__ == '__main__':
    
    app = QtGui.QApplication([])
    dialog = dialog_window()
    result = dialog.exec_()
    #if result: print 'OK Button pressed'
    #else: print 'Cancel Button pressed'