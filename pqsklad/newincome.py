from PyQt4 import QtCore, QtGui
import messages
import mdata
import incomeadd

def insmes(txt):
    return unicode(messages.messages[txt], "cp1251")

class dialog_window(QtGui.QDialog):
    title = " "
    date = "20081227"
    cur_prov = 0
    providers = []
    
    def __init__(self):
        QtGui.QDialog.__init__(self)
        self.setupUi()
   
    def setupUi(self):
        #self.tblProv.setTable(globals()['providers'])
        #self.tblRefer.setTable(globals()['reference'])

        self.setObjectName("dialog")
        self.resize(QtCore.QSize(QtCore.QRect(0,0,800,600).size()).expandedTo(self.minimumSizeHint()))
        self.setWindowTitle(self.title)
        
        self.buttonBox = QtGui.QDialogButtonBox(self)
        self.buttonBox.setGeometry(QtCore.QRect(450,560,341,32))
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtGui.QDialogButtonBox.Cancel|QtGui.QDialogButtonBox.NoButton|QtGui.QDialogButtonBox.Ok)
        
        self.label0 = QtGui.QLabel(self)
        self.label0.setGeometry(QtCore.QRect(10,0,211,16))
        self.label0.setText(insmes("ITEM_PROV"))
        self.PROV_comboBox = QtGui.QComboBox(self)
        self.PROV_comboBox.setGeometry(QtCore.QRect(10,20,300,22))
        self.providers = []
        i = -1
        for r in mdata.get_providers():
            i += 1
            self.providers.append([i, r['id']])
            self.PROV_comboBox.addItem(r['short_name'])

        #self.label0 = QtGui.QLabel(self)
        #self.label0.setGeometry(QtCore.QRect(320,0,211,16))
        #self.label0.setText(insmes("ITEM_PROV"))
        #self.incomeDate = QtGui.QDateEdit(self)
        #self.incomeDate.setCalendarPopup(True)
        #self.incomeDate.setGeometry(QtCore.QRect(320,20,100,22))
        
        btnAdd = QtGui.QPushButton(self)
        btnAdd.setGeometry(QtCore.QRect(720,15,32,32))
        btnAdd.setText("+")        
        btnAdd.setToolTip('Ctrl+Insert')

        btnDel = QtGui.QPushButton(self)
        btnDel.setGeometry(QtCore.QRect(760,15,32,32))
        btnDel.setText("-")        
        btnDel.setToolTip('Ctrl+Delete')
        
        self.label1 = QtGui.QLabel(self)
        self.label1.setGeometry(QtCore.QRect(10,50,211,16))
        self.label1.setText(insmes("TITLE_INCOME"))
        
        self.incomeTable = QtGui.QTableWidget(self)
        self.incomeTable.setGeometry(QtCore.QRect(10,70,780,480)) 
        self.incomeTable.setAlternatingRowColors(True)
        self.incomeTable.setEditTriggers(QtGui.QAbstractItemView.NoEditTriggers)
        
        
        QtCore.QObject.connect(self.buttonBox,QtCore.SIGNAL("accepted()"),self.saveIncome)
        QtCore.QObject.connect(self.buttonBox,QtCore.SIGNAL("rejected()"),self.reject)
        self.connect(self.PROV_comboBox, QtCore.SIGNAL("currentIndexChanged(int)"), self.setPROV)
        self.connect(btnAdd, QtCore.SIGNAL("clicked()"), self.add_item)
        btnAdd.setShortcut('Ctrl+Insert')
        self.connect(btnDel, QtCore.SIGNAL("clicked()"), self.del_item)
        btnDel.setShortcut('Ctrl+Delete')

        QtCore.QMetaObject.connectSlotsByName(self)
        self.markingTable()
        self.setPROV()
    
    def saveIncome(self):
        for row in xrange(self.incomeTable.rowCount()):
            item = QtGui.QTableWidgetItem()
            item = self.incomeTable.item(row, 2)
            product = item.text()
            item = self.incomeTable.item(row, 5)
            count = item.text()
            item = self.incomeTable.item(row, 6)
            price = item.text()
            item = self.incomeTable.item(row, 8)
            nds = item.text()
            print nds
            mdata.add_income(product, self.cur_prov, self.date, int(count), float(price), float(nds)) 
        self.accept()
    
    def add_item(self):
        dialog = incomeadd.dialog_window()
        result = dialog.exec_()
        if result:
            row_count = self.incomeTable.rowCount()
            self.incomeTable.setRowCount(row_count + 1)
            # add prod group ID 
            item = QtGui.QTableWidgetItem()
            item.setText(str(dialog.cur_product_group))
            self.incomeTable.setItem(row_count, 0, item)
            # add prod group 
            item = QtGui.QTableWidgetItem()
            item.setText(mdata.get_products_groups_by_id(dialog.cur_product_group))
            self.incomeTable.setItem(row_count, 1, item)
            # add name prod ID
            item = QtGui.QTableWidgetItem()
            item.setText(str(dialog.cur_product))
            self.incomeTable.setItem(row_count, 2, item)
            # add name prod
            item = QtGui.QTableWidgetItem()
            item.setText(mdata.get_product_name_by_id(dialog.cur_product))
            self.incomeTable.setItem(row_count, 3, item)
            # add unit prod
            item = QtGui.QTableWidgetItem()
            item.setText(mdata.get_product_unit_by_id(dialog.cur_product))
            self.incomeTable.setItem(row_count, 4, item)
            # add count
            item = QtGui.QTableWidgetItem()
            item.setText(str(dialog.count))
            self.incomeTable.setItem(row_count, 5, item)
            # add price
            item = QtGui.QTableWidgetItem()
            item.setText(str(dialog.price))
            self.incomeTable.setItem(row_count, 6, item)
            # add summa
            item = QtGui.QTableWidgetItem()
            summa = dialog.price* dialog.count
            item.setText(str(summa))
            self.incomeTable.setItem(row_count, 7, item)
            # add NDS
            item = QtGui.QTableWidgetItem()
            item.setText(str(dialog.nds))
            self.incomeTable.setItem(row_count, 8, item)


    def del_item(self):
        cur_row = self.incomeTable.currentRow()
        if cur_row < 0:
            return
        reply = QtGui.QMessageBox.question(self, '' ,
                    insmes("QMESS_DEL"), QtGui.QMessageBox.Yes, QtGui.QMessageBox.No)
        if reply == QtGui.QMessageBox.Yes:
            self.incomeTable.removeRow(cur_row)


    def markingTable(self):
        #self.incomeTable
        columns = (
        ("PROD_GROUP_ID", 0),
        ("PROD_GROUP", 200),
        ("PROD_NAME_ID", 0),
        ("PROD_NAME", 200),
        ("PROD_UNIT", 50),  
        ("ITEM_COUNT", 70),
        ("ITEM_PRICE", 100),
        ("ITEM_TOTAL_PRICE", 100),
        ("PROD_NDS", 50)
        )
        self.incomeTable.clear()
        self.incomeTable.setColumnCount(len(columns))
        self.incomeTable.setRowCount(0)
        for c in range(len(columns)):
            headerItem0 = QtGui.QTableWidgetItem()
            headerItem0.setText(insmes(columns[c][0])) 
            self.incomeTable.setHorizontalHeaderItem(c,headerItem0)
            self.incomeTable.horizontalHeader().resizeSection(c, columns[c][1])        
    
    def setPROV(self):
        #self.cur_product_group = self.product_group[self.PRGR_comboBox.currentIndex()][0]
        self.setWindowTitle(insmes("TITLE_INCOME") + " - " + self.PROV_comboBox.currentText())
        for i in self.providers:
            if i[0] == self.PROV_comboBox.currentIndex():
                self.cur_prov = i[1] 
         
    def setPROD(self):
        self.product_name = self.PROD_edit.text()
        self.setWindowTitle(self.product_name)
    def setPRUT(self):
        self.product_unit = self.PRUT_edit.text()

        
if __name__ == '__main__':
    
    app = QtGui.QApplication([])
    app.setStyle("Cleanlooks")
    
    dialog = dialog_window()
    result = dialog.exec_()
    #if result: print 'OK Button pressed'
    #else: print 'Cancel Button pressed'